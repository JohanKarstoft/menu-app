const weekMenu = [
    {
        day: 'Mandag',
        veggie: "Tofu",
        omni: "gris"
    },
    {
        day: 'Tirsdag',
        veggie: "Tofu",
        omni: "gris"
    },
    {
        day: 'Onsdag',
        veggie: "Tofu",
        omni: "gris"
    },
    {
        day: 'Torsdag',
        veggie: "Tofu",
        omni: "gris"
    },
    {
        day: 'Fredag',
        veggie: "Tofu",
        omni: "gris"
    }
];

export default weekMenu;