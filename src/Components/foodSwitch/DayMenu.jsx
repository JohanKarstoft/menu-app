import { Card } from 'antd';
import React from 'react';

const DayMenu = (props) => {
    return (
        <Card title={props.day} >
            {props.food}
        </Card>
    );
};