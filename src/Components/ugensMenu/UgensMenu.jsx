import { Table } from "antd";
import React from "react";

const UgensMenu = props => {
  const cells = props.menuData.map(dayMenu => {
    return (
      <>
        <tr>
          <td rowSpan="2">{dayMenu.ugeDag}</td>
          <td>Omnivores</td>
          <td>{dayMenu.omnivores}</td>
        </tr>
        <tr>
          <td>vegetarer</td>
          <td>{dayMenu.vegetarer}</td>
        </tr>
      </>
    );
  });

  return (
    <div className="ant-table">
      <table>
        <thead className="ant-table-thead">
          <tr>
            <th>Ugedag</th>
            <th>Menu</th>
          </tr>
        </thead>
        <tbody className="ant-table-tbody">{cells}</tbody>
      </table>
    </div>
  );
};
export default UgensMenu;
