import { Cascader, Select } from "antd";
import React from "react";

import { tempOptions } from "./options";

export const MenuCascader = () => {
  const { Option } = Select;

  const handleSelectChange = (value) => {
    console.log(value);
  };

  const handleCascaderChange = (value) => {
    console.log(value);
  };

  const selectMenu = (
    <Select
      style={{ width: '100%' }}
      onChange={handleSelectChange}
      getPopupContainer={() => document.getElementsByClassName("johans-popup-container-of-fun")[0]}
    >
      <Option value="tofu">Tofu</Option>
      <Option value="blomkaal">Blomkål</Option>
    </Select>
  );
  const options = tempOptions(selectMenu);
  return <Cascader
    options={options}
    onChange={handleSelectChange}
    popupClassName="johans-popup-container-of-fun"
  />;
};
