import { Select } from "antd";
import React from "react";
const { Option } = Select;


export const tempOptions = (selectMenu) => [
  {
    label: "Uge 1",
    value: "week1",
    children: [
      {
        label: "Mandag",
        value: "mandag",
        children: [
          {
            label: "Vegetar",
            value: "vegetar",
            children: [{ label: selectMenu }]
          },
          {
            label: "Ordinær",
            value: "ordinary",
            children: [
              <Select>
                <Option value="gris">Grissebasse</Option>
                <Option value="høne">Gokkedyr</Option>
              </Select>
            ]
          }
        ]
      },
      {
        label: "Tirsdag",
        value: "tirsdag",
        children: [
          {
            label: "Vegetar",
            value: "vegetar",
            children: [
              <Select>
                <Option value="tofu">Tofu</Option>
                <Option value="blomkaal">Blomkål</Option>
              </Select>
            ]
          },
          {
            label: "Ordinær",
            value: "ordinary",
            children: [
              <Select>
                <Option value="gris">Grissebasse</Option>
                <Option value="høne">Gokkedyr</Option>
              </Select>
            ]
          }
        ]
      },
      {
        label: "Onsdag",
        value: "onsdag",
        children: [
          {
            label: "Vegetar",
            value: "vegetar",
            children: [
              <Select>
                <Option value="tofu">Tofu</Option>
                <Option value="blomkaal">Blomkål</Option>
              </Select>
            ]
          },
          {
            label: "Ordinær",
            value: "ordinary",
            children: [
              <Select>
                <Option value="gris">Grissebasse</Option>
                <Option value="høne">Gokkedyr</Option>
              </Select>
            ]
          }
        ]
      },
      {
        label: "Torsdag",
        value: "torsdag",
        children: [
          {
            label: "Vegetar",
            value: "vegetar",
            children: [
              <Select>
                <Option value="tofu">Tofu</Option>
                <Option value="blomkaal">Blomkål</Option>
              </Select>
            ]
          },
          {
            label: "Ordinær",
            value: "ordinary",
            children: [
              <Select>
                <Option value="gris">Grissebasse</Option>
                <Option value="høne">Gokkedyr</Option>
              </Select>
            ]
          }
        ]
      },
      {
        label: "fredag",
        value: "fredag",
        children: [
          {
            label: "Vegetar",
            value: "vegetar",
            children: [
              <Select>
                <Option value="tofu">Tofu</Option>
                <Option value="blomkaal">Blomkål</Option>
              </Select>
            ]
          },
          {
            label: "Ordinær",
            value: "ordinary",
            children: [
              <Select>
                <Option value="gris">Grissebasse</Option>
                <Option value="høne">Gokkedyr</Option>
              </Select>
            ]
          }
        ]
      }
    ]
  }
  // {
  //   label: "Uge 2",
  //   value: "week2",
  //   children: [
  //     {
  //       label: "Mandag",
  //       value: "mandag",
  //       children: [
  //         {
  //           label: "Vegetar",
  //           value: "vegetar",
  //           children: [
  //             <Select>
  //               <Option value="tofu">Tofu</Option>
  //               <Option value="blomkaal">Blomkål</Option>
  //             </Select>
  //           ]
  //         },
  //         {
  //           label: "Ordinær",
  //           value: "ordinary",
  //           children: [
  //             <Select>
  //               <Option value="gris">Grissebasse</Option>
  //               <Option value="høne">Gokkedyr</Option>
  //             </Select>
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       label: "Tirsdag",
  //       value: "tirsdag",
  //       children: [
  //         {
  //           label: "Vegetar",
  //           value: "vegetar",
  //           children: [
  //             <Select>
  //               <Option value="tofu">Tofu</Option>
  //               <Option value="blomkaal">Blomkål</Option>
  //             </Select>
  //           ]
  //         },
  //         {
  //           label: "Ordinær",
  //           value: "ordinary",
  //           children: [
  //             <Select>
  //               <Option value="gris">Grissebasse</Option>
  //               <Option value="høne">Gokkedyr</Option>
  //             </Select>
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       label: "Onsdag",
  //       value: "onsdag",
  //       children: [
  //         {
  //           label: "Vegetar",
  //           value: "vegetar",
  //           children: [
  //             <Select>
  //               <Option value="tofu">Tofu</Option>
  //               <Option value="blomkaal">Blomkål</Option>
  //             </Select>
  //           ]
  //         },
  //         {
  //           label: "Ordinær",
  //           value: "ordinary",
  //           children: [
  //             <Select>
  //               <Option value="gris">Grissebasse</Option>
  //               <Option value="høne">Gokkedyr</Option>
  //             </Select>
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       label: "Torsdag",
  //       value: "torsdag",
  //       children: [
  //         {
  //           label: "Vegetar",
  //           value: "vegetar",
  //           children: [
  //             <Select>
  //               <Option value="tofu">Tofu</Option>
  //               <Option value="blomkaal">Blomkål</Option>
  //             </Select>
  //           ]
  //         },
  //         {
  //           label: "Ordinær",
  //           value: "ordinary",
  //           children: [
  //             <Select>
  //               <Option value="gris">Grissebasse</Option>
  //               <Option value="høne">Gokkedyr</Option>
  //             </Select>
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       label: "fredag",
  //       value: "fredag",
  //       children: [
  //         {
  //           label: "Vegetar",
  //           value: "vegetar",
  //           children: [
  //             <Select>
  //               <Option value="tofu">Tofu</Option>
  //               <Option value="blomkaal">Blomkål</Option>
  //             </Select>
  //           ]
  //         },
  //         {
  //           label: "Ordinær",
  //           value: "ordinary",
  //           children: [
  //             <Select>
  //               <Option value="gris">Grissebasse</Option>
  //               <Option value="høne">Gokkedyr</Option>
  //             </Select>
  //           ]
  //         }
  //       ]
  //     }
  //   ]
  // }
];
