import React, { useState } from "react";
import UgensMenu from "./Components/ugensMenu/UgensMenu";
import "antd/dist/antd.css";
import { MenuCascader } from "./Components/MenuCascader/MenuCascader";

function App() {
  const [menuData, setMenuData] = useState([
    {
      ugeDag: "Mandag",
      menu: {
        omnivores: "nomrlat mad",
        vegetarer: "Blomkål"
      }
    },
    {
      ugeDag: "Tirsdag",
      menu: {
        omnivores: "nomrlat mad",
        vegetarer: "Blomkål"
      }
    },
    {
      ugeDag: "Onsdag",
      menu: {
        omnivores: "nomrlat mad",
        vegetarer: "Blomkål"
      }
    },
    {
      ugeDag: "Torsdag",
      menu: {
        omnivores: "nomrlat mad",
        vegetarer: "Blomkål"
      }
    },
    {
      ugeDag: "Fredag",
      menu: {
        omnivores: "nomrlat mad",
        vegetarer: "Blomkål"
      }
    }
  ]);
  return (
    <div className="App">
      <MenuCascader />
      <UgensMenu menuData={menuData} />
    </div>
  );
}

export default App;
